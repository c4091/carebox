# Proyecto Bootcamp Make It Real

## Carebox

🚀 Link para ver el proyecto: (https://carebox.netlify.app/)

![Carebox](https://i.imgur.com/J7Q4dNl.png)

## Integrantes

<table>
  <tr>
      <td align="center">
      <a href="">
        <img src="https://avatars.githubusercontent.com/u/26855595?s=400&u=7b35b03e47bd4b51b28b9413b428c2071ab8cb94&v=4" width="100px;" alt=""/>
        <br />
        <sub><b>Jimena Ruiz</b></sub>
      </a>
      <br />
      <span>♌🥞🚲🕹</span>
    </td>
    <td align="center">
      <a href="https://jorge-vicuna.gitlab.io/jorge-vicuna/">
        <img src="https://jorge-vicuna.gitlab.io/jorge-vicuna/static/media/avatar.272f0e79.jpg" width="100px;" alt=""/>
        <br />
        <sub><b>Jorge Vicuña Valle</b></sub>
      </a>
            <br />
      <span>♌🍗🎸🏀</span>
    </td>
        <td align="center">
      <a href="profiles/luis-salcedo.md">
      <img src="https://avatars.githubusercontent.com/u/8843955?s=200&v=4" width="100px;" alt=""/>
      <br />
      <sub><b>Luis Salcedo</b></sub>
      </a>
      <br />
      <span>♉ 🍝 🎬 🕺</span>
    </td>
        <td align="center">
      <a href="profiles/david-valverde.md">
        <img src="https://avatars.githubusercontent.com/u/93108717?s=100" width="100px;" alt=""/>
        <br />
        <sub><b>David Valverde</b></sub>
      </a>
      <br />
      <span>♑🍔🎮📚</span>
    </td>
</Table>
