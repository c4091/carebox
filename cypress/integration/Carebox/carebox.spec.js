import "cypress-localstorage-commands";
import "cypress-file-upload";

describe("Pruduct tests", () => {
  beforeEach(() => {
    // cy.visit("http://localhost:3000");
  });

  it("shoul render home page ", () => {
    cy.restoreLocalStorage();
    cy.visit("http://localhost:3000");
    cy.wait(2000);
    cy.get('[data-test-id="login-page-button"]').click();
    cy.saveLocalStorage();
  });

  it("shoul render login page ", () => {
    cy.url().should("include", "/login");
    cy.get('[data-test-id="email-input"]').type("jorge150896@gmail.com");
    cy.get('[data-test-id="passwd-input"]').type("123456");
    cy.get('[data-test-id="login-button"]').click();
    cy.saveLocalStorage();
  });

  it("should show a perfil page", () => {
    cy.restoreLocalStorage();
    cy.url().should("include", "/perfil");

    cy.get('[data-test-id="perfil-form-elements"]')
      .children()
      .should("to.have.length.of.at.most", 5);
    cy.get('[data-test-id="perfil-button-editar/guardar"]').click();
    cy.get('[data-test-id="perfil-name-input"]').clear();
    cy.get('[data-test-id="perfil-name-input"]').type("Jorge Teofanes"); // para prueba cambiar aqui
    cy.get('[data-test-id="perfil-lastName-input"]').clear();
    cy.get('[data-test-id="perfil-lastName-input"]').type("Vicuna Valle"); // para prueba cambiar aqui
    cy.get('[data-test-id="perfil-button-editar/guardar"]').click();
    cy.saveLocalStorage();

    cy.get('[data-test-id="miBox-page-button"]').click();
    cy.on("uncaught:exception", (err, runnable) => {
      return false;
    });
  });

  it("should show a miBox page", () => {
    cy.restoreLocalStorage();
    cy.url().should("include", "/mibox");
    cy.wait(2000);

    cy.on("uncaught:exception", (err, runnable) => {
      return false;
    });
    cy.get('[data-test-id="mibox-filter-input"]').clear();
    cy.get('[data-test-id="mibox-filter-input"').type("box one 2022");
    cy.wait(2000);
    cy.get('[data-test-id="mibox-filter-button"]').click();
    cy.on("uncaught:exception", (err, runnable) => {
      return false;
    });
    cy.get('[data-test-id="mibox-filter-input"]').clear();
    cy.wait(500);
    cy.get('[data-test-id="mibox-filter-input"').type("box");
    cy.wait(2000);
    cy.get('[data-test-id="mibox-filter-button"]').click();
    cy.wait(3000);
    cy.get('[data-test-id="mibox-detalles-button"]').click();
    cy.wait(1000);

    cy.saveLocalStorage();
  });

  it("should show a miBoxId page", () => {
    cy.restoreLocalStorage();

    cy.url().should("include", "/details/6244c7c985476956dfc98016");
    cy.get('[data-test-id="miBoxId-elements"]')
      .children()
      .should("to.have.length.of.at.most", 5);
    cy.wait(3000);
    cy.get('[data-test-id="miBoxId-editar-button"]').click();
    cy.wait(3000);
    cy.saveLocalStorage();
  });

  it("should show a miBoxIdEdit page", () => {
    cy.restoreLocalStorage();

    cy.url().should("include", "/savebox/6244c7c985476956dfc98016");
    cy.on("uncaught:exception", (err, runnable) => {
      return false;
    });
    cy.get('[data-test-id="miBoxIdEdit-agregar-button"]').click();
    cy.wait(3000);
    cy.get('[data-test-id="miBoxIdEdit-popup-name-input"]').clear();
    cy.get('[data-test-id="miBoxIdEdit-popup-name-input"]').type("Dove");
    cy.wait(2000);

    cy.get('[data-test-id="miBoxIdEdit-popup-buscar-button"]').click();
    cy.wait(2000);
    cy.get('[data-test-id="miBoxIdEdit-popup-seleccionar-producto"]').click();
    cy.wait(2000);
    cy.get('[data-test-id="miBoxIdEdit-popup-agregar-producto"]').click();
    cy.wait(1500);
    cy.scrollTo("bottom");
    cy.wait(1500);
    cy.get('[data-test-id="miBoxIdEdit-eliminar-button"]').click();
    cy.scrollTo("top");
    cy.wait(1500);
    cy.wait(1500);
    cy.scrollTo("bottom");
    cy.wait(1500);
    cy.scrollTo("top");
    cy.wait(1500);
    cy.get('[data-test-id="miBoxIdEdit-editarName-input"]').clear();
    cy.get('[data-test-id="miBoxIdEdit-editarName-input"]').type(
      "box one Cambio"
    );
    cy.wait(1500);
    cy.scrollTo("top");
    cy.get('[data-cy="miBoxIdEdit-file-input"]').attachFile("reactjs.png");
    cy.wait(1500);
    cy.get('[data-test-id="miBoxIdEdit-guardar-button"]').click();
    cy.wait(1500);
    cy.scrollTo("top");
    cy.wait(1500);
    cy.saveLocalStorage();
  });

  it("should show a miBoxId page", () => {
    cy.restoreLocalStorage();

    cy.url().should("include", "/details/6244c7c985476956dfc98016");
    cy.get('[data-test-id="miBoxId-elements"]')
      .children()
      .should("to.have.length.of.at.most", 5);
    cy.wait(3000);
    cy.get('[data-test-id="miBoxId-regresar-button"]').click();
    cy.wait(3000);
    cy.saveLocalStorage();
  });

  //
  it("should show a miBox page", () => {
    cy.restoreLocalStorage();

    cy.url().should("include", "/mibox");
    cy.wait(2000);
    cy.on("uncaught:exception", (err, runnable) => {
      return false;
    });
    cy.get('[data-test-id="mibox-detalles-button"]').click();
    cy.wait(1000);

    cy.saveLocalStorage();
  });

  it("should show a miBoxId page", () => {
    cy.restoreLocalStorage();

    cy.url().should("include", "/details/6244c7c985476956dfc98016");
    cy.get('[data-test-id="miBoxId-elements"]')
      .children()
      .should("to.have.length.of.at.most", 5);
    cy.wait(3000);
    cy.get('[data-test-id="miBoxId-editar-button"]').click();
    cy.wait(3000);
    cy.saveLocalStorage();
  });
  //

  it("should show a miBoxIdEdit page", () => {
    cy.restoreLocalStorage();
    cy.url().should("include", "/savebox/6244c7c985476956dfc98016");
    cy.on("uncaught:exception", (err, runnable) => {
      return false;
    });
    cy.wait(1500);
    cy.scrollTo("top");
    cy.wait(1500);
    cy.get('[data-test-id="miBoxIdEdit-editarName-input"]').clear();
    cy.get('[data-test-id="miBoxIdEdit-editarName-input"]').type("box one");
    cy.wait(1500);
    cy.scrollTo("top");
    cy.wait(1500);
    cy.get('[data-cy="miBoxIdEdit-file-input"]').attachFile("flutter.png");
    cy.get('[data-test-id="miBoxIdEdit-guardar-button"]').click();
    cy.scrollTo("top");
    cy.wait(1500);
    cy.saveLocalStorage();
  });

  it("should show a miBoxId page", () => {
    cy.restoreLocalStorage();

    cy.url().should("include", "/details/6244c7c985476956dfc98016");
    cy.get('[data-test-id="miBoxId-elements"]')
      .children()
      .should("to.have.length.of.at.most", 5);
    cy.wait(3000);
    cy.get('[data-test-id="miBoxId-regresar-button"]').click();
    cy.wait(3000);
    cy.saveLocalStorage();
  });

  it("should show a miBox page", () => {
    cy.restoreLocalStorage();
    cy.url().should("include", "/mibox");
    cy.wait(2000);
    cy.on("uncaught:exception", (err, runnable) => {
      return false;
    });
    cy.get('[data-test-id="box-page-button"]').click();
    cy.wait(2000);

    cy.saveLocalStorage();
  });

  it("should show a Box page", () => {
    cy.restoreLocalStorage();

    cy.url().should("include", "/box");

    cy.get('[data-test-id="Box-editarName-input"]').clear();
    cy.get('[data-test-id="Box-editarName-input"]').type("Dental");
    cy.wait(1500);
    cy.get('[data-test-id="Box-filter-button"]').click();
    cy.scrollTo("top");
    cy.wait(3000);
    cy.get('[data-test-id="Box-filter-button"]').click();
    cy.wait(3000);
    // cy.get('[data-test-id="Box-precioMinimo-input"]').clear();
    cy.get('[data-test-id="Box-precioMinimo-input"]').type("30");
    // cy.get('[data-test-id="Box-precioMaximo-input"]').clear();
    cy.get('[data-test-id="Box-precioMaximo-input"]').type("40");
    cy.get('[data-test-id="Box-filter-button"]').click();
    cy.wait(3000);
    cy.get('[data-test-id="Box-filter-button"]').click();
    cy.wait(3000);
    cy.get('[data-test-id="mibox-detalles-button"]').click();
    cy.wait(2000);

    cy.saveLocalStorage();
  });

  it("should show a Boxdetail page", () => {
    cy.restoreLocalStorage();
    cy.url().should("include", "/details/621ee34345b7520e0ebd12bb");
    cy.wait(2000);
    cy.get('[data-test-id="home-page-button"]').click();
    cy.wait(2000);
    cy.saveLocalStorage();
  });

  it("should show a Home page", () => {
    cy.restoreLocalStorage();
    cy.url().should("include", "/");
    cy.wait(2000);
    cy.scrollTo("top");
    cy.wait(1500);
    cy.scrollTo("bottom");
    cy.wait(2000);
    cy.scrollTo("top");
  });
});
