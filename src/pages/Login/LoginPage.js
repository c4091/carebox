import React, { useContext, useState } from "react";
import { Fade } from "react-reveal";
import Formbasic from "../../components/Formbasic/Formbasic";
import { ReactComponent as Logo } from "../../assets/svg/logoCB.svg";
import { useNavigate } from "react-router-dom";
import { AuthContext } from "../../auth/authContext";
import { types } from "../../types/types";
import { loginUser } from "../../services/LoginService";
import { setToken } from "../../services/TokenService";
import { ToastContainer, toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";

import "./LoginPage.scss";

const LoginPage = () => {
  const navigate = useNavigate();
  const [loading, setLoading] = useState(false);
  const [error, setError] = useState(false);
  const { dispatch } = useContext(AuthContext);

  const getUserFromApi = (form) => {
    // registerUser({ email: form.email, password: form.password }).then(

    loginUser(form).then((data) => {
      const dataClient = data.client;

      if (data.status === 1) {
        setLoading(false);
        setToken("Bearer " + data.token);
        const userCarebox = {
          idClient: dataClient._id,
          names: dataClient.names,
          lastNames: dataClient.lastNames,
          phone: dataClient.phone,
          address: dataClient.address,
          userLogin: {
            email: dataClient.userLogin.email,
          },
        };
        const action = {
          type: types.login,
          payload: { userCarebox },
        };

        dispatch(action);
        // toast.success(`${data.message}`, {
        //   position: toast.POSITION.BOTTOM_LEFT,
        //   pauseOnHover: false,

        //   onClose: () => {

        //   },
        // });
        navigate("/perfil", {
          replace: true,
        });
      } else {
        setLoading(false);
        toast.error(`${data.message}`, {
          position: toast.POSITION.BOTTOM_LEFT,
          autoClose: 3000,
          pauseOnHover: false,
        });
        setError(data.message);
      }
    });
  };

  const handleLogin = async (form) => {
    setLoading(true);
    getUserFromApi(form);
  };

  return (
    <main>
      <div className="loginBox">
        <ToastContainer autoClose={1500} />

        <Fade>
          <div className="loginBox__left">
            <Formbasic
              handleLogin={handleLogin}
              loading={loading}
              error={error}
            />
          </div>
        </Fade>
        <div className="loginBox__right">
          <Fade up>
            <Logo fill="white" />
            <p className="graphicloginBox__info__text">
              Consigue tus productos de aseo <span>en un solo sitio</span>
            </p>
          </Fade>
        </div>
      </div>
    </main>
  );
};
export default LoginPage;
