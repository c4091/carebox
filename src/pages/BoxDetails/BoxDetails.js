import React, { useContext, useEffect, useState } from "react";
import { Link, useNavigate, useParams } from "react-router-dom";
import { AuthContext } from "../../auth/authContext";
import DivideComponent from "../../components/DivideComponent/DivideComponent";
import ProductCard from "../../components/ProductCard/ProductCard";
import SubscriptionDialog from "../../components/SubscriptionDialog/SubscriptionDialog";
import { getBoxById } from "../../services/BoxService";
import "./BoxDetails.scss";

const BoxDetails = () => {
  const { idBox, lastRoute } = useParams();
  const [box, setBox] = useState({ products: [] });
  const { user } = useContext(AuthContext);
  const navigate = useNavigate();

  const getBoxFromApi = async () => {
    getBoxById(idBox).then(data => {
      console.log(data);
      if (data.box) {
        setBox(data.box);
      }
    });
  };

  useEffect(() => {
    getBoxFromApi();
    // eslint-disable-next-line
  }, []);

  const LeftComponent = () => {
    const [open, setOpen] = useState(false);

    const handleClickOpen = () => {
      if (user.userCarebox !== undefined) {
        setOpen(true);
      } else {
        navigate("/login", {
          replace: true,
        });
      }
    };

    const handleClose = () => {
      setOpen(false);
    };

    const EditButton = () => {
      if (lastRoute === "mibox") {
        return (
          <div>
            <Link
              to={{
                pathname: `/mibox/savebox/${idBox}`,
              }}
              data-test-id="miBoxId-editar-button"
            >
              Editar box
              <div className="btn">
                <i
                  className="fa fa-pencil-square-o fa-2x"
                  aria-hidden="true"
                ></i>{" "}
              </div>
            </Link>
          </div>
        );
      } else return <></>;
    };

    return (
      <>
        {!!box.image && (
          <div>
            <img src={box.image} alt={box.name} width="100px" />
            <br />
          </div>
        )}

        <EditButton></EditButton>
        <button
          type="button"
          className="btn btn-care"
          onClick={handleClickOpen}
        >
          Suscribirme
        </button>
        <Link
          to={{
            pathname: `/${lastRoute}`,
          }}
        >
          <button
            type="button"
            className="btn btn-dark m-2"
            data-test-id="miBoxId-regresar-button"
          >
            Regresar
          </button>
        </Link>
        <SubscriptionDialog
          open={open}
          handleClose={handleClose}
          box={box}
        ></SubscriptionDialog>
      </>
    );
  };

  const RightComponent = () => {
    return (
      <>
        <div
          className="d-flex align-content-stretch flex-wrap"
          data-test-id="miBoxId-elements"
        >
          {box.products.map(product => {
            return (
              <ProductCard
                key={product._id}
                product={product}
                canDelete={false}
                deleteProduct={null}
              ></ProductCard>
            );
          })}
        </div>
      </>
    );
  };

  return (
    <>
      <DivideComponent
        left={{
          title: `${box.name}`,
          subtitle: `S/. ${box.price}`,
          component: <LeftComponent />,
        }}
        right={{
          title: "Productos",
          subtitle: `${box.products.length} resultados`,
          component: <RightComponent />,
        }}
      ></DivideComponent>
    </>
  );
};

export default BoxDetails;
