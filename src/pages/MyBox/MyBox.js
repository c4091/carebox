import TextField from "@mui/material/TextField";
import React, { useEffect, useState } from "react";
import { Card } from "react-bootstrap";
import { Link } from "react-router-dom";
import addIcon from "../../assets/add.png";
import BoxCard from "../../components/BoxCard/BoxCard";
import DivideComponent from "../../components/DivideComponent/DivideComponent";
import { filterBoxes, getBoxes } from "../../services/MyBoxService";
import "./MyBox.scss";
import { useContext } from "react";
import { AuthContext } from "../../auth/authContext";

const MyBox = () => {
  const [boxes, setBoxes] = useState([]);
  const { user } = useContext(AuthContext);

  const getBoxesFromApi = async () => {
    console.log(user);
    getBoxes(user.userCarebox.idClient).then(data => {
      if (data.boxes) {
        setBoxes(data.boxes);
      }
    });
  };

  useEffect(() => {
    getBoxesFromApi();
    // eslint-disable-next-line
  }, []);

  const LeftComponent = () => {
    const [filters, setFilters] = useState({
      name: "",
      priceMin: "",
      priceMax: "",
    });

    const handleInputChange = event => {
      let filtersAux = { ...filters, [event.target.name]: event.target.value };
      setFilters(filtersAux);
    };

    const filterBoxesFromApi = async () => {
      filterBoxes(user.userCarebox.idClient, filters).then(data => {
        if (data.boxes) {
          setBoxes(data.boxes);
        }
      });
    };

    return (
      <>
        <label className="mb-3">Filtrar por Nombre</label>
        <TextField
          data-test-id="mibox-filter-input"
          id="textFiledName"
          size="small"
          label="Nombre del box"
          variant="outlined"
          name="name"
          fullWidth
          type="text"
          className="mb-3"
          value={filters.name}
          onChange={handleInputChange}
        />
        <button
          data-test-id="mibox-filter-button"
          type="button"
          className="btn btn-care"
          onClick={filterBoxesFromApi}
        >
          Filtrar
        </button>
      </>
    );
  };

  const RightComponent = () => {
    const NewCard = () => {
      return (
        <div className="newcard">
          <Link
            to={{
              pathname: `/mibox/savebox/0`,
            }}
          >
            <Card>
              <Card.Img
                className="newcard__image"
                variant="top"
                src={addIcon}
              />
              <Card.Body className="card-body">
                <Card.Title>Nuevo Box</Card.Title>
              </Card.Body>
            </Card>
          </Link>
        </div>
      );
    };

    return (
      <>
        <div className="d-flex align-content-stretch flex-wrap mibox">
          <NewCard></NewCard>
          {boxes.map(box => {
            return (
              <BoxCard key={box.idBox} box={box} lastRoute={"mibox"}></BoxCard>
            );
          })}
        </div>
      </>
    );
  };

  return (
    <>
      <DivideComponent
        left={{
          title: "Box Personal",
          subtitle: "Consulta tus boxs o crea uno nuevo.",
          component: <LeftComponent />,
        }}
        right={{
          title: "Mi Box",
          subtitle: `${boxes.length} resultados`,
          component: <RightComponent />,
        }}
      ></DivideComponent>
    </>
  );
};

export default MyBox;
