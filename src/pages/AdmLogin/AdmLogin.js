import { css } from "@emotion/react";
import { createTheme, ThemeProvider } from "@mui/material/styles";
import TextField from "@mui/material/TextField";
import React, { useContext, useState } from "react";
import { useNavigate } from "react-router-dom";
import RingLoader from "react-spinners/RingLoader";
import { toast, ToastContainer } from "react-toastify";
import { ReactComponent as Logo } from "../../assets/svg/logoCB.svg";
import { AuthContext } from "../../auth/authContext";
import { loginAdminUser } from "../../services/LoginAdminService";
import { types } from "../../types/types";
import "./AdmLogin.scss";

const theme = createTheme({
  palette: {
    primary: {
      // Purple and green play nicely together.
      //   main: purple[500],
      main: "#E0E0E0",
    },
    secondary: {
      // This is green.A700 as hex.
      main: "#662D91",
    },
    disabled: {
      // Purple and green play nicely together.
      //   main: purple[500],
      main: "#E0E0E0",
    },
  },
});
const override = css`
  display: block;
  margin: 0 auto;
  border-color: "#fff";
`;

const AdmLogin = () => {
  const navigate = useNavigate();
  const [loading, setLoading] = useState(false);
  // eslint-disable-next-line
  const [error, setError] = useState(false);

  const { dispatch } = useContext(AuthContext);

  const getUserAdminFromApi = form => {
    // registerUser({ email: form.email, password: form.password }).then(

    loginAdminUser(form).then(data => {
      const dataAdmin = data.administrator;

      if (data.status === 1) {
        setLoading(false);
        const adminCarebox = {
          idClient: dataAdmin._id,
          names: dataAdmin.names,
          lastNames: dataAdmin.lastNames,
          phone: dataAdmin.phone,
          address: dataAdmin.address,
          userLogin: {
            email: dataAdmin.userLogin.email,
          },
        };
        const action = {
          type: types.loginAdmin,
          payload: { adminCarebox },
        };

        dispatch(action);

        navigate("/admin/suscripcionesDiarias", {
          replace: true,
        });
      } else {
        setLoading(false);
        toast.error(`${data.message}`, {
          position: toast.POSITION.BOTTOM_LEFT,
          autoClose: 3000,
          pauseOnHover: false,
        });
        setError(data.message);
      }
    });
  };

  const handleLoginAdmin = async form => {
    setLoading(true);
    getUserAdminFromApi(form);
  };

  const [form, setForm] = useState({ email: "", password: "" });
  const handleForm = e => {
    setForm({
      ...form,
      [e.target.name]: e.target.value,
    });
  };

  return (
    <>
      <div className="adminLogin">
        <ToastContainer autoClose={1500} />

        <div className="adminLogin__form">
          <Logo className="adminLogin__form__logo" fill="white" />
          <div className="adminLogin__form__tittle">Iniciar Sesión</div>
          <div className="adminLogin__form__subtittle">
            Ingresa tus credenciales
          </div>
          <form
            onSubmit={e => {
              e.preventDefault();
              handleLoginAdmin(form);
            }}
          >
            <ThemeProvider theme={theme}>
              <TextField
                id="outlined-basic"
                label="Email"
                variant="outlined"
                name="email"
                fullWidth
                type="email"
                className="mb-5 textfield"
                value={form.email}
                placeholder="Ingresa tu correo electrónico"
                focused
                disabled={loading}
                onChange={handleForm}
              />
              <TextField
                id="outlined-basic"
                label="Password"
                variant="outlined"
                name="password"
                fullWidth
                type="password"
                placeholder="Ingresa tu contraseña"
                className="mb-5"
                focused
                value={form.password}
                disabled={loading}
                onChange={handleForm}
              />
            </ThemeProvider>
            <div className="adminLogin__form__button">
              {form.password === "" || form.email === "" ? (
                <button type="button" className="button" disabled>
                  Ingresar
                </button>
              ) : (
                <button
                  // type="button"
                  className="button"
                  // onClick={() => {
                  //   handleLogin(form);
                  // }}
                  disabled={loading}
                >
                  {loading ? (
                    <RingLoader color={"#fff"} css={override} size={40} />
                  ) : (
                    "Ingresar"
                  )}
                </button>
              )}
            </div>
          </form>
        </div>
      </div>
    </>
  );
};
export default AdmLogin;
