import TextField from "@mui/material/TextField";
import React, { useEffect, useState } from "react";
import BoxCard from "../../components/BoxCard/BoxCard";
import DivideComponent from "../../components/DivideComponent/DivideComponent";
import { filterBoxes, getBoxes } from "../../services/BoxService";
import "./Box.scss";

const Box = () => {
  const [boxes, setBoxes] = useState([]);

  const getBoxesFromApi = async () => {
    getBoxes().then((data) => {
      if (data.boxes) {
        setBoxes(data.boxes);
      }
    });
  };

  useEffect(() => {
    getBoxesFromApi();
  }, []);

  const LeftComponent = () => {
    const [filters, setFilters] = useState({
      name: "",
      priceMin: "",
      priceMax: "",
    });

    const handleInputChange = (event) => {
      let filtersAux = { ...filters, [event.target.name]: event.target.value };
      setFilters(filtersAux);
    };

    const filterBoxesFromApi = async () => {
      filterBoxes(filters).then((data) => {
        console.log(data);
        if (data.boxes) {
          setBoxes(data.boxes);
        }
      });
    };

    return (
      <>
        <label className="mb-3">Filtrar por Nombre</label>
        <TextField
          data-test-id="Box-editarName-input"
          id="textFiledName"
          size="small"
          label="Nombre del box"
          variant="outlined"
          name="name"
          fullWidth
          type="text"
          className="mb-3"
          value={filters.name}
          onChange={handleInputChange}
        />
        <label className="mb-3">Filtrar por Precio</label>
        <TextField
          data-test-id="Box-precioMinimo-input"
          size="small"
          label="Precio mínimo"
          variant="outlined"
          name="priceMin"
          fullWidth
          type="number"
          className="mb-3"
          value={filters.priceMin}
          onChange={handleInputChange}
        />
        <TextField
          data-test-id="Box-precioMaximo-input"
          size="small"
          label="Precio máximo"
          variant="outlined"
          name="priceMax"
          fullWidth
          type="number"
          className="mb-3"
          value={filters.priceMax}
          onChange={handleInputChange}
        />
        <button
          data-test-id="Box-filter-button"
          type="button"
          className="btn btn-care"
          onClick={filterBoxesFromApi}
        >
          Filtrar
        </button>
      </>
    );
  };

  const RightComponent = () => {
    return (
      <>
        <div className="d-flex align-content-stretch flex-wrap">
          {boxes.map((box) => {
            return (
              <BoxCard key={box._id} box={box} lastRoute={"box"}></BoxCard>
            );
          })}
        </div>
      </>
    );
  };

  return (
    <>
      <DivideComponent
        left={{
          title: "Box Standard",
          subtitle: "Elige el Box que se acomode mejor a tus necesidades.",
          component: <LeftComponent />,
        }}
        right={{
          title: "Box",
          subtitle: `${boxes.length} resultados`,
          component: <RightComponent />,
        }}
      ></DivideComponent>
    </>
  );
};

export default Box;
