import { Grid } from "@mui/material";
import Dialog from "@mui/material/Dialog";
import MenuItem from "@mui/material/MenuItem";
import Select from "@mui/material/Select";
import TextField from "@mui/material/TextField";
import React, { useEffect, useState } from "react";
import { Card } from "react-bootstrap";
import {
  filterProducts,
  getCategories,
  getProducts,
} from "../../services/ProductService";
import ProductCardSelect from "../ProductCardSelect/ProductCardSelect";
import "./ProductDialog.scss";
import FormControl from "@mui/material/FormControl";
import InputLabel from "@mui/material/InputLabel";

const ProductDialogCard = ({ handleClose, addProducts }) => {
  const [filters, setFilters] = useState({ name: "", idCategory: 0 });

  const [products, setProducts] = useState([]);
  const [categories, setCategories] = useState([]);

  // eslint-disable-next-line
  const [selectedProducts, setSelectedProducts] = useState([]);

  const getProductsFromApi = () => {
    getProducts().then(data => {
      if (data.products) {
        setProducts(data.products);
      }
    });
  };

  const getCategoriesFromApi = () => {
    getCategories().then(data => {
      if (data.categories) {
        setCategories(data.categories);
      }
    });
  };

  useEffect(() => {
    getProductsFromApi();
    getCategoriesFromApi();
  }, []);

  const handleInputChange = event => {
    let filterAux = {
      ...filters,
      [event.target.name]: event.target.value,
    };
    setFilters(filterAux);
  };

  const filterProductsFromApi = () => {
    filterProducts(filters).then(data => {
      if (data.products) {
        setProducts(data.products);
      }
    });
  };

  const addProductsFromDialog = () => {
    addProducts(selectedProducts);
    handleClose();
  };

  const selectProduct = product => {
    selectedProducts.push(product);
  };

  const deselectProduct = product => {
    selectedProducts.pop(product);
  };

  return (
    <div>
      <Card>
        <Card.Body>
          <Card.Title className="mb-4">
            <h2>Nuestros Productos</h2>
          </Card.Title>
          <Grid container>
            <Grid item className="margin-right">
              <TextField
                data-test-id="miBoxIdEdit-popup-name-input"
                size="small"
                label="Nombre"
                variant="outlined"
                name="name"
                type="text"
                className="mb-3"
                value={filters.name}
                onChange={handleInputChange}
              />
            </Grid>
            <Grid item className="margin-right">
              <FormControl sx={{ maxWidth: 250, minWidth: 250 }}>
                <InputLabel id="demo-simple-select-helper-label">
                  Categoría
                </InputLabel>
                <Select
                  size="small"
                  className="mb-3"
                  labelId="demo-simple-select-helper-label"
                  id="demo-simple-select-helper"
                  value={filters.idCategory}
                  label="Categoría"
                  onChange={handleInputChange}
                  name="idCategory"
                >
                  <MenuItem value={0}>Todas las Categorías</MenuItem>
                  {categories.map(category => {
                    return (
                      <MenuItem key={category._id} value={category._id}>
                        {category.name}
                      </MenuItem>
                    );
                  })}
                </Select>
              </FormControl>
            </Grid>
            <Grid item xs={2}>
              <button
                data-test-id="miBoxIdEdit-popup-buscar-button"
                IdEdit-popup-buscar-button
                type="button"
                className="button mb-3 btn btn-care"
                onClick={filterProductsFromApi}
              >
                <i className="fa fa-search" aria-hidden="true"></i>&nbsp; Buscar
              </button>
            </Grid>
          </Grid>
          <hr />
          <label>Selecciona los productos y luego haz click en Agregar</label>

          <Grid item xs={2}>
            <button
              data-test-id="miBoxIdEdit-popup-agregar-producto"
              type="button"
              className="mb-3 btn btn-success"
              onClick={addProductsFromDialog}
            >
              <i className="fa fa-plus" aria-hidden="true" />
              &nbsp;&nbsp;Agregar
            </button>
          </Grid>

          <Grid container>
            <div className="products-list">
              {products.map(product => {
                return (
                  <ProductCardSelect
                    key={product._id}
                    product={product}
                    selectProduct={selectProduct}
                    deselectProduct={deselectProduct}
                  ></ProductCardSelect>
                );
              })}
            </div>
          </Grid>
          <br></br>
          <Grid item xs={2}>
            <button
              type="button"
              className="btn btn btn-dark"
              onClick={handleClose}
            >
              <i className="fa fa-chevron-left" aria-hidden="true"></i>
              &nbsp;&nbsp;Regresar
            </button>
          </Grid>
        </Card.Body>
      </Card>
    </div>
  );
};

const ProductDialog = props => {
  const { open, handleClose, addProducts } = props;
  return (
    <Dialog open={open} onClose={handleClose} maxWidth="xl">
      <ProductDialogCard
        handleClose={handleClose}
        addProducts={addProducts}
      ></ProductDialogCard>
    </Dialog>
  );
};

export default ProductDialog;
