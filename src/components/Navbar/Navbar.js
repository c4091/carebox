import React, { useState, useEffect, useContext } from "react";
import { Link, NavLink, useLocation } from "react-router-dom";
// import logo from "../../assets/svg/logo.svg";
import { ReactComponent as Logo } from "../../assets/svg/logoCB.svg";
import { ReactComponent as NavIcon } from "../../assets/svg/navicon.svg";
import { useNavigate } from "react-router-dom";
import { AuthContext } from "../../auth/authContext";
import { types } from "../../types/types";

import "./Navbar.scss";

const Navbar = ({ ubication }) => {
  const [validatePerfil, setValidatePerfil] = useState();
  const navigate = useNavigate();

  let location = useLocation();
  let position = location.pathname;

  const { user, dispatch } = useContext(AuthContext);
  useEffect(() => {
    setValidatePerfil(position.includes("/perfil"));
    // eslint-disable-next-line
  }, []);

  const handleLogout = () => {
    dispatch({ type: types.logout });

    navigate("/login", {
      replace: true,
    });
  };

  const handlePerfil = () => {
    navigate("/perfil", {
      replace: true,
    });

    setValidatePerfil(true);
  };
  return (
    <nav
      className={`${ubication} navbar fixed-top navbar-expand-md navbar-dark`}
    >
      <Link
        className="navbar-brand"
        to="/"
        onClick={() => setValidatePerfil(false)}
      >
        {/* <img src={logo} className="navbar-logo" alt="" /> */}
        <Logo className="navbar-logo" fill="white" />
        Carebox
      </Link>
      <button
        className="navbar-toggler collapsed"
        type="button"
        data-bs-toggle="collapse"
        data-bs-target="#navbarNavDropdown"
        aria-controls="navbarNavDropdown"
        aria-expanded="false"
        aria-label="Toggle navigation"
      >
        <span>
          <NavIcon />
        </span>
      </button>
      {/* <div className="navbar-bg"></div> */}
      <div className="collapse navbar-collapse" id="navbarNavDropdown">
        <div className="navbar-nav">
          <NavLink
            className={({ isActive }) =>
              "nav-item nav-link " + (isActive ? "active" : "")
            }
            onClick={() => setValidatePerfil(false)}
            to="/"
            data-test-id="home-page-button"
          >
            Inicio
          </NavLink>
          <NavLink
            className={({ isActive }) =>
              "nav-item nav-link " + (isActive ? "active" : "")
            }
            onClick={() => setValidatePerfil(false)}
            to="/box"
            data-test-id="box-page-button"
          >
            Box
          </NavLink>
          {user.userCarebox !== undefined ? (
            <NavLink
              className={({ isActive }) =>
                "nav-item nav-link " + (isActive ? "active" : "")
              }
              onClick={() => setValidatePerfil(false)}
              to="/mibox"
              data-test-id="miBox-page-button"
            >
              Mi Box
            </NavLink>
          ) : (
            ""
          )}
          {user.userCarebox !== undefined ? (
            <NavLink
              className={({ isActive }) =>
                "nav-item nav-link " + (isActive ? "active" : "")
              }
              to="/suscripciones"
              onClick={() => setValidatePerfil(false)}
            >
              Suscripciones
            </NavLink>
          ) : (
            ""
          )}
          {user.userCarebox !== undefined ? (
            <li className="nav-item dropdown">
              <NavLink
                className="nav-link dropdown-toggle"
                data-bs-toggle="dropdown"
                aria-expanded="false"
                id="navbarDropdown"
                to="/x"
              >
                <span className={`${validatePerfil ? "avtivare" : ""} `}>
                  {user.userCarebox === undefined
                    ? "Ingresar"
                    : user.userCarebox.names}
                </span>
              </NavLink>
              <ul className="dropdown-menu" aria-labelledby="navbarDropdown">
                <li>
                  <button className="dropdown-item" onClick={handlePerfil}>
                    Mi Perfil
                  </button>
                </li>
                {/* <li>
        <hr className="dropdown-divider" />
      </li> */}
                <li>
                  <button className="dropdown-item" onClick={handleLogout}>
                    Cerrar Sesion
                  </button>
                </li>
              </ul>
            </li>
          ) : (
            <NavLink
              className={({ isActive }) =>
                "nav-item nav-link " + (isActive ? "active" : "")
              }
              to="/login"
              onClick={() => setValidatePerfil(false)}
              data-test-id="login-page-button"
            >
              Ingresar
            </NavLink>
          )}
        </div>
      </div>
    </nav>
  );
};
export default Navbar;
