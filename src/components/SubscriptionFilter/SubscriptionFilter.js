import { createTheme, ThemeProvider } from "@mui/material/styles";
import { grey } from "@mui/material/colors";
import TextField from "@mui/material/TextField";
import React, { useState } from "react";
import "./SubscriptionFilter.scss";

const theme = createTheme({
  palette: {
    primary: {
      main: grey[400],
    },
  },
});

const SubscriptionFilter = ({ handleSearch }) => {
  const [email, setEmail] = useState("");

  const handleInputChange = (event) => {
    setEmail(event.target.value);
  };

  const search = () => {
    console.log(email);
    if (email !== "") {
      handleSearch(email);
    }
  };

  return (
    <>
      <div className="adminSup__section__right__subtittle">
        <label className="adminSup__section__right__subtittle__label">
          Filtro de Búsqueda
        </label>
      </div>
      <div className="row adminSup__section__right__data">
        <ThemeProvider theme={theme}>
          <div className=" col col-12 col-md-5">
            <TextField
              id="outlined-required"
              label="Email"
              variant="outlined"
              name="email"
              fullWidth
              type="email"
              className="mb-4"
              placeholder="Buscar por correo electrónico"
              onChange={handleInputChange}
              value={email}
            />
          </div>
        </ThemeProvider>
        <div className=" col col-12 col-md-2">
          <button type="button" className="button" onClick={search}>
            Buscar
          </button>
        </div>
      </div>
    </>
  );
};

export default SubscriptionFilter;
