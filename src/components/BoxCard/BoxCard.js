import React from "react";
import { Card } from "react-bootstrap";
import "./BoxCard.scss";
import { ReactComponent as BoxIcon } from "../../assets/svg/boxicon.svg";
import { Link } from "react-router-dom";

const BoxCard = props => {
  const { box, lastRoute } = props;
  return (
    <div className="boxcard">
      <Card>
        {!!box.image ? (
          <div className="imgContainerBox">
            <img src={box.image} alt={box.name} />
          </div>
        ) : (
          <div className="imgContainerBox">
            <BoxIcon />
          </div>
        )}

        <Card.Body className="card-body">
          <Card.Title>{box.name}</Card.Title>
          <Card.Text>S/. {box.price}</Card.Text>
          <Link
            to={{
              pathname: `/${lastRoute}/details/${box._id}`,
            }}
          >
            <button
              data-test-id={
                box._id === "6244c7c985476956dfc98016" ||
                box._id === "621ee34345b7520e0ebd12bb"
                  ? "mibox-detalles-button"
                  : ""
              }
              className="btn btn-care btn-block"
            >
              Detalles
            </button>
          </Link>
        </Card.Body>
      </Card>
    </div>
  );
};

export default BoxCard;
