import httpClient from "../utils/httpClient";
import { getToken } from "./TokenService";

export const createSubscription = async (subscription) => {
  const data = await httpClient
    .post("/subscriptions/create", subscription, {
      headers: {
        Authorization: getToken(),
      },
    })
    .then((v) => v.data);
  return data;
};

export const getSubscriptions = async (idClient) => {
  const data = await httpClient
    .get(`/subscriptions/listByIdClient?idClient=${idClient}`, {
      headers: {
        Authorization: getToken(),
      },
    })
    .then((v) => v.data);
  return data;
};

export const getAllSubscriptions = async () => {
  const data = await httpClient
    .get(`/subscriptions/listAll`, {
      headers: {
        Authorization: getToken(),
      },
    })
    .then((v) => v.data);
  return data;
};

export const getAllSubscriptionsByEmail = async (email) => {
  const data = await httpClient
    .get(`/subscriptions/listAllByEmail?email=${email}`, {
      headers: {
        Authorization: getToken(),
      },
    })
    .then((v) => v.data);
  return data;
};

export const cancelSubscription = async (idSubscription) => {
  const data = await httpClient
    .delete(`/subscriptions/cancel?idSubscription=${idSubscription}`, {
      headers: {
        Authorization: getToken(),
      },
    })
    .then((v) => v.data);
  return data;
};

export const getTodaySubscriptions = async () => {
  const data = await httpClient
    .get(`/subscriptions/listTodaySubscriptions`, {
      headers: {
        Authorization: getToken(),
      },
    })
    .then((v) => v.data);
  return data;
};

export const getTodaySubscriptionsByEmail = async (email) => {
  const data = await httpClient
    .get(`/subscriptions/listTodaySubscriptionsByEmail?email=${email}`, {
      headers: {
        Authorization: getToken(),
      },
    })
    .then((v) => v.data);
  return data;
};

export const sendDelivery = async (idSubscription) => {
  const data = await httpClient
    .put(
      `/subscriptions/update`,
      {
        idSubscription: idSubscription,
      },
      {
        headers: {
          Authorization: getToken(),
        },
      }
    )
    .then((v) => v.data);
  return data;
};
