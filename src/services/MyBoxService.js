import httpClient from "../utils/httpClient";
import { getToken } from "./TokenService";

export const getBoxes = async (idClient) => {
  const data = await httpClient
    .get(`/boxes/listCustom?idClient=${idClient}`, {
      headers: {
        Authorization: getToken(),
      },
    })
    .then((v) => v.data);
  return data;
};

export const filterBoxes = async (idClient, filters) => {
  const data = await httpClient
    .get(`/boxes/listCustomByName?idClient=${idClient}&name=${filters.name}`, {
      headers: {
        Authorization: getToken(),
      },
    })
    .then((v) => v.data);
  return data;
};

export const getBoxById = async (idBox) => {
  const data = await httpClient
    .get(`/boxes/listById?idBox=${idBox}`)
    .then((v) => v.data);
  return data;
};

export const createBox = async (box) => {
  const data = await httpClient
    .post("/boxes/createCustom", box, {
      headers: {
        Authorization: getToken(),
      },
    })
    .then((v) => v.data);
  return data;
};

export const updateBox = async (box) => {
  const data = await httpClient
    .put("/boxes/updateBox", box, {
      headers: {
        Authorization: getToken(),
      },
    })
    .then((v) => v.data);
  return data;
};

export const uploadImageBox = async (file) => {
  let formData = new FormData();
  formData.append("image", file);
  const data = await httpClient
    .post("/boxes/uploadImage", formData, {
      headers: {
        "Content-Type": "multipart/form-data",
      },
    })
    .then((v) => v.data);
  return data;
};
