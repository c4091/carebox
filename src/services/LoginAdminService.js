import httpClient from "../utils/httpClient";

export const loginAdminUser = async (login) => {
  const data = await httpClient
    .post("/login/administrator", login)
    .then((v) => {
      return v.data;
    });
  return data;
};
