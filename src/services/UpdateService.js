import httpClient from "../utils/httpClient";
import { getToken } from "./TokenService";

export const updateUser = async (login) => {
  const data = await httpClient
    .put("/clients/update", login, {
      headers: {
        Authorization: getToken(),
      },
    })
    .then((v) => {
      return v.data;
    });
  return data;
};
