import httpClient from "../utils/httpClient";
import { getToken } from "./TokenService";

export const getProducts = async () => {
  const data = await httpClient
    .get("/products", {
      headers: {
        Authorization: getToken(),
      },
    })
    .then((v) => v.data);
  return data;
};

export const getCategories = async () => {
  const data = await httpClient
    .get("/categories", {
      headers: {
        Authorization: getToken(),
      },
    })
    .then((v) => v.data);
  return data;
};

export const filterProducts = async (filters) => {
  if (filters.name === "" && filters.idCategory !== 0) {
    const data = await httpClient
      .get(`/products/listByIdCategory?idCategory=${filters.idCategory}`, {
        headers: {
          Authorization: getToken(),
        },
      })
      .then((v) => v.data);
    return data;
  } else if (filters.name !== "" && filters.idCategory === 0) {
    const data = await httpClient
      .get(`/products/listByName?name=${filters.name}`, {
        headers: {
          Authorization: getToken(),
        },
      })
      .then((v) => v.data);
    return data;
  } else if (filters.name !== "" && filters.idCategory !== 0) {
    const data = await httpClient
      .get(
        `/products/listByIdCategoryAndName?idCategory=${filters.idCategory}&name=${filters.name}`,
        {
          headers: {
            Authorization: getToken(),
          },
        }
      )
      .then((v) => v.data);
    return data;
  } else {
    return getProducts();
  }
};
