import httpClient from "../utils/httpClient";

export const loginUser = async (login) => {
  const data = await httpClient.post("/login/client", login).then((v) => {
    return v.data;
  });
  return data;
};

export const restorePassword = async (email) => {
  const data = await httpClient
    .post('/login/restoreClientPassword', { email: email })
    .then((v) => {
      return v.data;
    });
  return data;
};
