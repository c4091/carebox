import axios from "axios";

export const getValidation = async (idReference) => {
  const data = await axios
    .get(` https://secure.epayco.co/validation/v1/reference/${idReference}`)
    .then((v) => v.data);
  return data;
};
