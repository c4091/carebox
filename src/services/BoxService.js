import httpClient from "../utils/httpClient";

export const getBoxes = async () => {
  const data = await httpClient.get("/boxes/listStandard").then((v) => v.data);
  return data;
};

export const filterBoxes = async (filters) => {
  if (
    filters.name === "" &&
    (filters.priceMin !== "" || filters.priceMax !== "")
  ) {
    const data = await httpClient
      .get(
        `/boxes/listStandardByMinPriceAndMaxPrice?maxPrice=${filters.priceMax}&minPrice=${filters.priceMin}`
      )
      .then((v) => v.data);
    return data;
  } else if (
    (filters.priceMin === "" || filters.priceMax === "") &&
    filters.name !== ""
  ) {
    const data = await httpClient
      .get(`/boxes/listStandardByName?name=${filters.name}`)
      .then((v) => v.data);
    return data;
  } else if (
    filters.name !== "" &&
    (filters.priceMin !== "" || filters.priceMax !== "")
  ) {
    const data = await httpClient
      .get(
        `/boxes/listStandardByNameAndMinPriceAndMaxPrice?name=${filters.name}&maxPrice=${filters.priceMax}&minPrice=${filters.priceMin}`
      )
      .then((v) => v.data);
    return data;
  } else {
    return getBoxes();
  }
};

export const getBoxById = async (idBox) => {
  const data = await httpClient
    .get(`/boxes/listById?idBox=${idBox}`)
    .then((v) => v.data);
  return data;
};
