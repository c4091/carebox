import { useEffect, useReducer } from "react";
import { AppRouter } from "./routers/AppRouter.js";
import { AuthContext } from "./auth/authContext";
import { authReducer } from "./auth/authReducer";

const init = () => {
  // return {
  //   logged: true,
  //   name: "Jorge VIcuna",
  // };
  return JSON.parse(localStorage.getItem("userCarebox")) || { logged: false };
};

function App() {
  const [user, dispatch] = useReducer(authReducer, {}, init);

  useEffect(() => {
    if (!user) return;
    localStorage.setItem("userCarebox", JSON.stringify(user));
  }, [user]);

  return (
    <>
      <AuthContext.Provider
        value={{
          user,
          dispatch,
        }}
      >
        <AppRouter />
      </AuthContext.Provider>
    </>
  );
}

export default App;
